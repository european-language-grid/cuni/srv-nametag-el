# NameTag Entity Linking service Docker image for ELG

This repository is a `docker` image builder for NameTag Entity Linking service for ELG.

The content of this repository is available under MPL 2.0 license,
but note that the NameTag models themselves are under CC BY-NC-SA.
