#!/usr/bin/env python3
import argparse
import collections
import gzip
import json
import math
import pickle
import re

import aiohttp
import aiohttp.web
import numpy as np
import stop_words
import ufal.nametag
import ufal.udpipe

class NameTagElModel:
    def __init__(self, model, verbose):
        self._verbose = verbose

        if model == "cs":
            self.lemmatize = True
            self.nametag = ufal.nametag.Ner.load("models/czech-cnec2.0-140304-no_numbers.ner")
            self.udpipe = ufal.udpipe.Model.load("models/czech-pdt-ud-2.5-191206.udpipe")
        elif model == "en":
            self.lemmatize = False
            self.nametag = ufal.nametag.Ner.load("models/english-conll-140408.ner")
        else:
            raise ValueError(f"Unknown language {model}")

        with gzip.open(f"models/{model}-kb-170603.pickle", "rb") as kb_file:
            self.kb = pickle.load(kb_file)
        self.kb_df = {}
        for word in self.kb.pop().split("\t", maxsplit=2)[2].split(" "):
            if "|" in word:
                word, count = word.split("|", maxsplit=1)
                count = int(count)
            else:
                count = 1
            self.kb_df[word] = count

        with gzip.open(f"models/{model}-index-170603.pickle", "rb") as index_file:
            index = pickle.load(index_file)
        self.index, self.index_lemmas = {}, {}
        while index:
            line = index.pop()
            forms, lemmas = line.split("\t", maxsplit=2)[:2]
            forms_lower = forms.lower()
            value = self.index.setdefault(forms_lower, line)
            if value != line:
                self.index[forms_lower] = value + ("\n" + line)
            if self.lemmatize:
                lemmas_lower = lemmas.lower()
                value = self.index_lemmas.setdefault(lemmas_lower, line)
                if value != line:
                    self.index_lemmas[lemmas_lower] = value + ("\n" + line)

        with open("models/el_weights-170603.json", "r") as weights_file:
            self.weights = json.load(weights_file)
        self.stop_words = set(stop_words.get_stop_words("cz" if model == "cs" else model))

    def _tokenize(self, text):
        forms = ufal.nametag.Forms()
        tokens = ufal.nametag.TokenRanges()
        tokenizer = self.nametag.newTokenizer()
        tokenizer.setText(text)
        sentences, ranges = [], []
        while tokenizer.nextSentence(forms, tokens):
            sentences.append(list(forms))
            ranges.append([(rng.start, rng.start + rng.length) for rng in tokens])
        return sentences, ranges

    def _lemmatize(self, sentences):
        result = []
        for words in sentences:
            sentence = ufal.udpipe.Sentence()
            for word in words:
                sentence.addWord(word)
            self.udpipe.tag(sentence, self.udpipe.DEFAULT)
            result.append([w.lemma for w in sentence.words[1:]])
        return result

    def _ner(self, sentences, lemmas, ranges):
        result = []
        for sentence, lemmas, ranges in zip(sentences, lemmas, ranges):
            forms = ufal.nametag.Forms()
            for word in sentence:
                forms.push_back(word)

            entities = ufal.nametag.NamedEntities()
            self.nametag.recognize(forms, entities)
            entities = [(entity.start, entity.start + entity.length) for entity in entities]
            entities = sorted(entities, key=lambda entity: (entity[0], -entity[1]))
            last_end = 0
            for entity in entities:
                if entity[0] >= last_end:
                    result.append((ranges[entity[0]][0], ranges[entity[1] - 1][1], sentence[entity[0]:entity[1]], lemmas[entity[0]:entity[1]]))
                    last_end = entity[1]

        return result

    def _find_candidates(self, mention, index, index_column, candidates, counts):
        mention_orig = " ".join(mention)
        mention_lc = mention_orig.lower()
        mention_title = mention_orig.title()
        mention_title1 = mention_title[:1] + mention_lc[1:]
        index = index.get(mention_lc, None)
        if index is None:
            return
        for line in index.split("\n"):
            columns = line.split("\t")
            text, links = columns[index_column], columns[2]
            matches = (["lower"] +
                       (["orig"] if mention_orig == text else []) +
                       (["title"] if mention_title == text or mention_title1 == text else []))
            for link in links.split(" "):
                candidate, kind = link.split("|", maxsplit=1)
                candidate, kind = int(candidate), kind.split(",")
                values = candidates.setdefault(candidate, {})
                for match in matches:
                    key = f"{kind[0][:1]}-{kind[0][1:].upper() or '_'}-{match}"
                    values[key] = values.get(key, 0) + 1
                if kind[0].startswith("A") and len(kind) >= 3:
                    count, total = int(kind[1]), int(kind[2])
                    for match in matches:
                        counts[match] = count + counts.get(match, 0)
                        match_candidate = f"{match}-{candidate}"
                        counts[match_candidate] = count + counts.get(match_candidate, 0)
                        counts[f"{match_candidate}-total"] = total * (2 if self.lemmatize else 1)

    def _best_candidate(self, candidates, counts, doc_bow, mention_bow):
        def candidate_score(candidate):
            return sum(self.weights[feature] * value for feature, value in candidates[candidate].items())

        if self._verbose:
            logs = {}
            for c in candidates:
                logs[c] = {"scores": {"mto": candidate_score(c)}, "mto": dict(candidates[c].items())}

        # Compute anchor text prior probabilities
        for candidate in candidates:
            values = candidates[candidate]
            for match in ["lower", "orig", "title"]:
                count = counts.get(f"{match}-{candidate}", 0)
                if count:
                    link_count = counts.get(match, 1)
                    anchor_total = counts.get(f"{match}-{candidate}-total", 1)

                    count_log, anchor_total_log = math.log1p(count), math.log1p(anchor_total)
                    values[f"AP1-_-{match}"] = math.log1p(count / anchor_total_log)
                    values[f"AP2-_-{match}"] = count_log / anchor_total_log
                    values[f"AP3-_-{match}"] = count / anchor_total
                    values[f"AP4-_-{match}"] = count / link_count
                    values[f"A1-l-_-{match}"] = count_log
                    values[f"A2-l-_-{match}"] = anchor_total_log

        if self._verbose:
            for c in candidates:
                logs[c]["scores"]["atpp"] = candidate_score(c) - logs[c]["scores"]["mto"]

        # Compute contextual similarity
        doc_bow_max = max(doc_bow.values(), default=1)
        for candidate in candidates:
            values = candidates[candidate]
            entity_bow = {}
            for word in self.kb[candidate].split("\t", maxsplit=2)[2].split(" "):
                if "|" in word:
                    word, count = word.split("|", maxsplit=1)
                    count = int(count)
                else:
                    count = 1
                entity_bow[word] = count
            entity_bow_sum = sum(entity_bow.values())
            entity_bow_max = max(entity_bow.values(), default=1)

            ws = list(entity_bow.keys() & doc_bow.keys() - mention_bow)
            if ws:
                entity_ws, doc_ws = np.array([entity_bow[w] for w in ws]), np.array([doc_bow[w] for w in ws])
                tfs = [
                    (1, entity_ws * doc_ws),
                    (2, (1 + np.log1p(entity_ws)) * (1 + np.log1p(doc_ws))),
                    (3, (0.5 + 0.5 * entity_ws / entity_bow_max) * (0.5 + 0.5 * doc_ws / doc_bow_max))
                ]
                docs, w_df = len(self.kb), np.array([self.kb_df[w] for w in ws])
                idfs = [
                    (0, np.ones(len(ws))),
                    (1, np.log1p(docs / w_df)),
                    (3, np.log1p(1 + docs / w_df)),
                    (5, np.log1p((docs - w_df) / w_df)),
                ]
                idfs.extend([
                    (2, np.square(idfs[1][1])),
                    (4, np.square(idfs[2][1])),
                    (6, np.square(idfs[3][1])),
                ])
                norms = [
                    (0, 1),
                    (1, entity_bow_sum),
                    (2, math.log1p(entity_bow_sum)),
                    (3, math.log1p(len(entity_bow))),
                    (4, len(entity_bow)),
                ]
                for tf_i, tf in tfs:
                    for idf_i, idf in idfs:
                        value = np.dot(tf, idf)
                        for norm_i, norm in norms:
                            values[f"{tf_i}-{idf_i}-{norm_i}"] = value / norm

            if self._verbose:
                logs[candidate]["bow"] = {"entity": entity_bow, "common": ws}
                logs[candidate]["scores"]["cs"] = candidate_score(candidate) - sum(logs[candidate]["scores"].values())

        if self._verbose:
            print(*sorted(((*self.kb[c].split("\t", maxsplit=2)[1::-1], candidate_score(c), c, logs[c]) for c in candidates), key=lambda v: v[2]),
                  "-" * 79, sep="\n")

        # Choose the best
        return max(candidates, key=candidate_score)

    def run(self, text):
        sentences, ranges = self._tokenize(text)
        lemmas = self._lemmatize(sentences) if self.lemmatize else sentences
        mentions = self._ner(sentences, lemmas, ranges)

        # Compute document bow
        doc_bow = [(word.lower(), lemma.lower()) for sentence, lemmas in zip(sentences, lemmas) for word, lemma in zip(sentence, lemmas)]
        doc_bow = [(word, lemma) for word, lemma in doc_bow if len(word) >= 2 and word not in self.stop_words and any(c.isalpha() for c in word)]
        doc_bow = [term for word, lemma in doc_bow for term in [word, *([lemma] if self.lemmatize and len(lemma) >= 3 else [])]]
        doc_bow = collections.Counter(doc_bow)

        result = []
        for (start, end, words, lemmas) in mentions:
            if self._verbose:
                print("Mention:", words, *(["Lemmas:", lemmas] if self.lemmatize else []))

            # Find candidates
            candidates, counts = {}, {}
            self._find_candidates(words, self.index, 0, candidates, counts)
            if self.lemmatize:
                self._find_candidates(lemmas, self.index_lemmas, 1, candidates, counts)

            if candidates:
                mention_bow = set(map(str.lower, words + (lemmas if self.lemmatize else [])))
                candidate = self._best_candidate(candidates, counts, doc_bow, mention_bow)
                result.append((start, end, ":".join(self.kb[candidate].split("\t", maxsplit=2)[:2])))

        return result


class NameTagElServer:
    class ProcessingError(Exception):
        def __init__(self, code, text, *params):
            self.code = code
            self.text = text
            self.params = params

        @staticmethod
        def InternalError(text):
            return NameTagElServer.ProcessingError("elg.service.internalError", "Internal error during processing: {0}", text)

        @staticmethod
        def InvalidRequest():
            return NameTagElServer.ProcessingError("elg.request.invalid", "Invalid request message")

        @staticmethod
        def TooLarge():
            return NameTagElServer.ProcessingError("elg.request.too.large", "Request size too large")

        @staticmethod
        def UnsupportedMime(mime):
            return NameTagElServer.ProcessingError(
                "elg.request.text.mimeType.unsupported", "MIME type {0} not supported by this service", mime)

        @staticmethod
        def UnsupportedType(request_type):
            return NameTagElServer.ProcessingError(
                "elg.request.type.unsupported", "Request type {0} not supported by this service", request_type)


    def __init__(self, verbose):
        self._verbose = verbose
        self._models = {}

    def _error_message(self, message):
        return {
            "failure": {
                "errors": [{
                    "code": message.code,
                    "text": message.text,
                    "params": message.params,
                }]
            }
        }

    def _annotation_message(self, annotations):
        return {
            "response": {
                "type": "annotations",
                "annotations": {
                    "nametag_el/{}".format(key): annotations[key] for key in annotations
                }
            }
        }

    async def _parse_elg_request(self, request):
        try:
            if request.content_type == "text/plain":
                return await request.text()
            elif request.content_type == "application/json":
                body = await request.text()
                try:
                    request = json.loads(body)
                except json.JSONDecodeError:
                    raise self.ProcessingError.InvalidRequest()

                if not isinstance(request, dict):
                    raise self.ProcessingError.InvalidRequest()

                if request.get("type", None) != "text":
                    raise self.ProcessingError.UnsupportedType(request.get("type", "<missing>"))

                if request.get("mimeType", "text/plain") != "text/plain":
                    raise self.ProcessingError.UnsupportedMime(request["mimeType"])

                return request.get("content", "")
            else:
                raise self.ProcessingError.UnsupportedMime(request.content_type)
        except aiohttp.web.HTTPRequestEntityTooLarge:
            raise self.ProcessingError.TooLarge()

    def _run_model(self, model, text):
        if model not in self._models:
            self._models[model] = NameTagElModel(model, self._verbose)
        model = self._models[model]

        entities = model.run(text)

        annotations = {}
        for start, end, link in entities:
            annotations.setdefault(link, []).append({"start": start, "end": end, "features": {}})

        return annotations

    def _json_response(self, json):
        return aiohttp.web.json_response(json, headers={"Access-Control-Allow-Origin": "*"})

    async def process_elg_request(self, request):
        model = request.match_info.get("model", "")
        try:
            content = await self._parse_elg_request(request)
            annotations = self._run_model(model, content)
            return self._json_response(self._annotation_message(annotations))

        except self.ProcessingError as e:
            return self._json_response(self._error_message(e))
        except:
            return self._json_response(self._error_message(self.ProcessingError.InternalError("Internal service error")))


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--max_request_size", default=1000000, type=int, help="Maximum request size")
    parser.add_argument("--port", default=8080, type=int, help="Port where to start the REST server")
    parser.add_argument("--verbose", default=False, action="store_true", help="Verbose logging")
    args = parser.parse_args()

    async def server(args):
        adapter = NameTagElServer(args.verbose)

        # Allow triple the limit in getting the request -- it might use %XY encoding.
        server = aiohttp.web.Application(client_max_size=3 * args.max_request_size)
        server.add_routes([aiohttp.web.post("/nametag_el/{model}", adapter.process_elg_request)])
        return server

    aiohttp.web.run_app(server(args), port=args.port)
