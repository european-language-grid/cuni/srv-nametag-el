#!/bin/sh

set -e

cd $(dirname $0)

curl https://lindat.mff.cuni.cz/repository/xmlui/bitstream/handle/11858/00-097C-0000-0023-7D42-8/czech-cnec-140304.zip -O
unzip -j czech-cnec-140304.zip czech-cnec-140304/czech-cnec2.0-140304-no_numbers.ner
rm czech-cnec-140304.zip

curl https://lindat.mff.cuni.cz/repository/xmlui/bitstream/handle/11234/1-3118/english-conll-140408.zip -O
unzip -j english-conll-140408.zip english-conll-140408/english-conll-140408.ner
rm english-conll-140408.zip

curl https://lindat.mff.cuni.cz/repository/xmlui/bitstream/handle/11234/1-3131/czech-pdt-ud-2.5-191206.udpipe -O

for el_data in LICENSE.CC-BY-NC-SA-4 cs-index-170603.pickle cs-kb-170603.pickle en-index-170603.pickle en-kb-170603.pickle; do
  curl https://ufallab.ms.mff.cuni.cz/~straka/nametag_el/$el_data -O
done
