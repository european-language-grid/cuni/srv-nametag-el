# Use an official Python 3.7 runtime with slim Debian Bullseye
FROM python:3.7-slim-bullseye

# Set the working directory to /nametag-el
WORKDIR /nametag-el

# Copy the repository files to /nametag-el
COPY LICENSE README.md nametag-el /nametag-el/

# Download the models
RUN apt-get update
RUN apt-get install -y curl unzip
RUN /bin/sh models/get_models.sh

# Install the Python requirements
RUN apt-get install -y g++ \
 && pip3 install -r requirements.txt \
 && apt-get purge --auto-remove -y g++

# Expose the service port
EXPOSE 8080

# Run the service when the container launches
CMD ["/usr/local/bin/python3", "nametag_el_server.py"]
